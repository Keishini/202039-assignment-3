#include <stdio.h>


int main()
 {
 	int j,i,num;
 	printf("Enter an integer:");
 	scanf("%d",&num);
 	printf("Multiplication tables from 1 to %d \n",num);
 	for(i=1;i<=10;i++)
 	{
 		for(j=1;j<=num;j++)
 		{
 			if(j<=num-1)
 			printf("   %d * %d =  %d  ",j,i,i*j);
 			else
 			printf("   %d * %d =  %d  ",j,i,i*j);
		 }
		 printf("  \n  \n");
	 }
	return 0;
}
